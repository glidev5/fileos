var bksp = function () {
    editor.remove("left");
}

var processInput;
$(window).load(function () {

    // ==========================
    // Middleman (stuff to communicate between keyboard and editor)
    // ==========================
    prevEndIndx = 0;
    processInput = function (keyboard) {
        var last = keyboard.last;
        console.log(last.key)

        // var newInput = last.val.substring( prevEndIndx, last.end )
        if (last.key === "shift") {

        }else if (last.key==="enter"){
          editor.insert("\n");
        } else {
            editor.insert(last.key);
        }


        // Prepare for the next go around
        prevEndIndx = last.end;
        // Why not, since we're skipping 'accept' anyway? Will be easier for people using keyboards.
        editor.focus();

        return keyboard;
        //return false;
    }; // End processInput()
    // ==========================
    // Keyboard
    // ==========================
    $('#text').keyboard({
        keyBinding: 'mousedown touchstart',
        autoAccept: true,
        alwaysOpen: false,
        position: {
            of: null,
            my: 'center bottom',
            at: 'center bottom',
            at2: 'center bottom'
        },
        reposition: false,
        initialFocus: true,
        stayOpen: true,
        layout: 'qwerty',
        useCombos: false,
        customLayout: {
            'default': ['` 1 2 3 4 5 6 7 8 9 0 - = {bksp}', '{tab} q w e r t y u i o p [ ] \\', 'a s d f g h j k l ; \' {enter}', '{shift} z x c v b n m , . / {shift}', '{meta1} {space} {left} {right} {up} {down} {extender} {cancel}'],
            'shift': ['~ ! @ # $ % ^ & * ( ) _ + {bksp}', '{tab} Q W E R T Y U I O P { } |', 'A S D F G H J K L : " {enter}', '{shift} Z X C V B N M < > ? {shift}', '{meta1} {space} {left} {right} {up} {down} {extender} {cancel}'],
            'meta1': ['` 1 2 3 4 5 6 7 8 9 0 - = {bksp}', '{home} {up} {end} {pageup}', '{left} {down} {right} {pagedown}', '', '{win} {space}']
        },
        change: function (evnt, keyboard, elem) {
            processInput(keyboard);
        }
    })


    // .addNavigation({
    //   // set start position [row-number, key-index]
    //   position   : [0,0],
    //   // true = navigate the virtual keyboard, false = navigate in input/textarea
    //   toggleMode : false,
    //   // css class added to the keyboard when toggle mode is on
    //   focusClass : 'hasFocus'
    //
    //   // **** deprecated ****
    //   // now set by $.keyboard.navigationKeys.toggle;
    //   // default set as 112 (event.which value for function 1 key)
    //   // toggleKey  : null
    // });

    $.extend($.keyboard.keyaction, {
        bksp: function (base) {
            editor.remove("left");
            return false;
        },
        left: function (base) {
            editor.navigateLeft(1);
        },
        right: function (base) {
            editor.navigateRight(1);
        },
        cancel: function (base) {
            $('#text').getkeyboard().close();
        },
        tab: function (base) {
            editor.indent();
            return false;
        },
        space: function (base) {
            editor.insert(" ");
            return false;
        },
        accept:function(base){
          $("#submit").click();
        }
    });

    var keyboard = $('#text').getkeyboard().close();
    $('#keyboard').on("click", function () {
        $('#text').getkeyboard().reveal();
        $(".ui-keyboard-preview").hide()
    });
});

//                '{Escape} {F1} {F2} {F3} {F4} {F5} {F6} {F7} {F8} {F9} {F10} {F11} {F12} {Insert} {Delete}',
