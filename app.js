var grequire=require('grequire');
var lib=require(__dirname+"/lib.js");
var log=lib.log;
var fneval=lib.fneval;
var express = require('express');
var app = express();
var child, load, config, text, html_option,ssh_option, file, port,secMgr;
var section=lib.section;
var tryLog=lib.tryLog;
var https=require("https");

global.vars={};
global.vars.port=port=80;
global.vars.file=file=process.argv[2];
global.secMgr=global.vars.secMgr=secMgr={};
global.that=this;
global.vars.express=express;
global.vars.app=app;

// main thread
// first loads config, then does load, these must run a bit later cause grequire is not fully loaded
setTimeout(function(){
  load(function(e,r){
    makeserver(function(e,r){
      log("Session Terminated.");
      process.exit();
    })
  })
},10);

// load in all the configs
function load(cb){
  //**config**
  config=grequire.requireFolder(__dirname,'ini');
  var config1=config['.fileos'];
  var config2=config['.shorthand'];
  config=config1;
  config.shorthand=_.defaults(config2.shorthand,config1.shorthand);

  //config.section.sectionPattern = new RegExp(config.section.sectionPattern,"gm");
  config.section.sectionPattern=fneval(config.section.sectionPattern);

  html_option   = fneval(config.html.option.trim());
  ssh_option    = fneval(config.ssh.option.trim());

  //log(config)
  //try{
    config.globalcmd  = _.map(config.globalcmd,fneval);
    config.sectioncmd = _.map(config.sectioncmd,fneval);
  //}catch(e){
  //  log(e)
  //}

  var shorts={};
  _.map(config.shorthand,function(value,index){
    shorts[index]=value;
  });
  config.shorthand=shorts;
  //log(config.shorthand)

  logcooldown=config.main.logcooldown;

  config.main.testmode=false;
  try{
    config.main.testmode=JSON.parse(process.argv[3]);
  }catch(e){}
  config.main.nextwatch=true;

  global.vars.config=config;

  log("checkSectionPattern: "+lib.checkSectionPattern());

  cb();
};
global.vars.load=load;

// run server for web portal
function makeserver(cb){
  //**server**

  lib.tryLogFile(".motd");

  log("watching file: "+global.vars.file);

  //lib.evalRequire(__dirname+"/server.js", this);
  app=require(__dirname+'/server.js')(app);

  var options = {
      key: fs.readFileSync('./ssl/privatekey.pem'),
      cert: fs.readFileSync('./ssl/certificate.pem'),
  };

  //app.listen(port, function () {
  //  log('fileos server listening on port '+port+'!');
  //});
  
  var httpServer=http.createServer(app);
  var httpsServer=https.createServer(options,app);
  httpServer.listen(port);
  httpsServer.listen(443);
  
  log('fileos server listening on port '+port+'!');
  log('fileos https server listening on port '+443+'!');

  //**watch**
  async.whilst(function(){return config.main.nextwatch;}, function(cb){
    async.waterfall([
      function(cb){
        Q(global.vars.file).then(watch).then(function(e,r){cb();});
      },
      function(cb){
        Q().then(continueWatch).then(function(e){
          e?process.exit():cb();
        })
      }
    ],function(e,r){
      cb();
    })
  }, function(e,r){
    cb();
  });
}

// clean up current watch cycle and ready for next if not in testmode
function continueWatch(){
  //log("continueWatch")
  var deferred=Q.defer();
  if(config.main.testmode){
    deferred.resolve(true);
  }else{
    setTimeout(function(){deferred.resolve(false);},global.vars.config.main.watchcooldown);
  }
  return deferred.promise;
}
// regenerate os.txt
function regenos(secMgr,cb){

  var content="";

  async.eachSeries(secMgr,function(sec,cb){
    content+=sec.format().trim().replace(/\\n/g,'\n')+"\n";
    return cb();
  },function(e,r){
    return fs.writeFile(file,content,cb)
  });
}

// run section command
function sectionCmdProc(o){
  log("sectionCmdProc")
  log(global.vars.secMgr)
  var deferred=Q.defer();
  async.eachSeries(global.vars.secMgr,function(sec,cb){
    log(sec)
    async.eachSeries(config.sectioncmd,function(cmd,cb){
      log(cmd)
      //log(!!sec)
      //log(!!(sec.content.trim()))
      //log(!o.executed)
      //log(cmd.pattern)
      //log(sec.raw)
      if((!!sec)&&(!!(sec.content.trim()))&&(!o.executed)&&(!!(o.args=cmd.pattern.exec(sec.raw)))){
        log(o.args)
        o.executed=true;
        o.sec=sec;
        o.cmd=cmd;
        o.type="section";
        //log(o.cmd.cmd)
        //log(o)
        cmdProc(o,function(e,o2){
          o=o2;
          cb();
        });
      }else{
        cb();
      }
    },function(e,r){
      cb();
    });
  },function(e,r){
    //log(o.data)
    deferred.resolve(o)
  });
  return deferred.promise;
}

// run global command
function globalCmdProc(o){
  log("globalCmdProc")
  //log(o)
  var deferred=Q.defer();
  async.eachSeries(config.globalcmd,function(cmd,cb){
    if(!o.executed&&(o.args=cmd.pattern.exec(o.data))){
      o.executed=true;
      o.type="global";
      o.cmd=cmd;
      cmdProc(o,function(e,o2){
        o=o2;
        cb();
      });
    }else{
      cb();
    }
  },
  function(e,o2){
    deferred.resolve(o);
  })
  return deferred.promise;
}

// run single command by variable
function cmdProc(o,cb){
  log("cmdProc")
  log(o.cmd.cmd)
  lib[o.cmd.cmd](o,function(e,o2){
    cb(null,o2);
  });
}

function writeWatchProc(o){
  log("writeWatchProc")
  //log(o)
  var deferred=Q.defer();
  if(o.executed){
    tryLog(o.data);
    fs.writeFile(global.vars.file,o.data,function(e,r){
      tryLog(e);
      setTimeout(function(e,r){deferred.resolve(o)},global.vars.config.main.fscooldown);
    })
  }else{
    setTimeout(function(e,r){deferred.resolve(o)},global.vars.config.main.fscooldown);
  }
  return deferred.promise;
}

function watch(file){
  var deferred=Q.defer();
  if(lib.filechanged(file)){
    fs.readFile(file,function(err,data){
      data=data.toString();
      data=data.replace(/\\n/g,"\n");

      //log(data)

      // main loop for watch method
      // analyze section into secMgr
      // then does sectioncmd
      // then does globalcmd
      async.waterfall([
        function(cb){
          section({ data:data,
            secMgr:secMgr,
            executed:false
          })
          .then(sectionCmdProc)
          .then(globalCmdProc)
          .then(writeWatchProc)
          .then(function(e,r){
            cb();
          });
        }
      ],function(e,r){
        deferred.resolve();
      })
    });
  }
  else{
    deferred.resolve();
  }
  return deferred.promise;
}
