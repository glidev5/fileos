// this is the unit test for fileos
var exec = require('child_process').exec;

//**unit test**
function log(obj){
  try{
    console.log(JSON.stringify(obj,null,2));
  }catch(e){}
}

var assert=require('assert');
var Mocha=require('mocha');
var Test=Mocha.Test;
var grequire=require('grequire');
var testfile="test/test.txt";

describe("Fileos",function(cb){
  // section format
  // sh cmd
  it('should be able to watch once',function(done){
    fs.writeFileSync(testfile,"{{sh echo 123}}");
    setTimeout(function(){
      runcmd("node app.js "+testfile+" true",function(e,r){
        //console.log(fs.readFileSync(testfile).toString())
        var result=!!((fs.readFileSync(testfile).toString())
          .match(/\*\*\*\*(.*)\*\*\*\*echo 123\*\*\*\*\n123\n\n\*\*\*\*\*\*\*\*/gm));
        //log(result);
        assert(result);
        done();
      });
    },400);
  })

  // save
  it('should be able to save to a file',function(done){
    fs.writeFileSync(testfile,"****HyxrMkO4S****echo 123****\n123{{save test/test2.txt}}\n\n********");
    setTimeout(function(){
      runcmd("node app.js test/test.txt true",function(e,r){
        setTimeout(function(){
          runcmd("node app.js test/test.txt true",function(e,r){
            var result=!!(fs.readFileSync("test/test2.txt").toString())
              .match(/\*\*\*\*(.*)\*\*\*\*echo 123\*\*\*\*([\s\S]*)123([\s\S]*)\*\*\*\*\*\*\*\*/gm);
            //log(result);
            assert(result);
            done();
          });
        },100);
      });
    },100);
  })

  // new
  it('should be able to create new section',function(done){
    fs.writeFileSync(testfile,"{{new}}");
    setTimeout(function(){
      runcmd("node app.js test/test.txt true",function(e,r){
        setTimeout(function(){
            var result=!!(fs.readFileSync(testfile).toString())
              .match(/\*\*\*\*(.*)\*\*\*\*new\*\*\*\*\n\n\*\*\*\*\*\*\*\*/gm);
            //log(result);
            assert(result);
            done();
        },100);
      });
    },100);
  })

//remove
  it('should be able to remove section',function(done){
    fs.writeFileSync(testfile,"\n****HyxrMkO4S****echo 123****\n123 {{remove}}\n\n********\n");
    setTimeout(function(){
      runcmd("node app.js test/test.txt true",function(e,r){
        setTimeout(function(){
          console.log(fs.readFileSync(testfile).toString())
            var result=(fs.readFileSync(testfile).toString().trim()=="");
            //log(result);
            assert(result);
            done();
        },500);
      });
    },100);
  })
})

// new
// remove
// copy
// reload


// run a single command and return command stdout
function runcmd(cmd,cb){
  log("running command: "+cmd)
  child = exec(cmd, function (error, stdout, stderr) {
    return cb(error,stdout);
  });
}

function log(str){
  console.log(JSON.stringify(str,null,2));
}
