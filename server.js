
var htmlToText = require('html-to-text');
var bodyParser = require('body-parser');
var jade = require('jade');
var less = require('less');
var lib=require(__dirname+'/lib.js');
var log=lib.log;
var auth = require('http-auth');

module.exports=function(app){
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
//app.use(global.vars.express.basicAuth('admin', '888'));

// Authentication module.
var basic = auth.basic({
    realm: "fileos",
    file: __dirname + "/users.htpasswd" // gevorg:gpass, Sarah:testpass ...
});

var options = {
    key: fs.readFileSync('./ssl/privatekey.pem'),
    cert: fs.readFileSync('./ssl/certificate.pem'),
};

app.use(auth.connect(basic));

// cleans www and reply as plain text
app.get('/www',function(req,res){
 needle.get(req.param("url"),html_option, function(error, response) {
  if (!error){
    text = htmlToText.fromString(response.body, {
        wordwrap: 80
    });
    res.send(text);
    log(text);
  }
 });
});

// returns json as plain text
app.get('/json',function(req,res){
 needle.get(req.param("url"),html_option, function(error, response) {
  if (!error){
    text = JSON.stringify(JSON.parse(response.body),null,2);
    res.send(text);
    log(text);
  }
 });
});

// return wiki result for the query
app.get('/wiki',function(req,res){
 needle.get("https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&titles="+req.param("query"),html_option, function(error, response) {
  if (!error){
    var text = JSON.stringify(response.body,null,2);
    res.send(text);
    log(text);
  }
  else {
    log(error);
  }
 });
});

// return jade file content
app.get('/jade',function(req,res){
  log(req.query.page)
  var jadetext=fs.readFileSync(__dirname+'/public/jade/'+req.query.page+".jade").toString();
  var ostext=fs.readFileSync(global.vars.file).toString().replace(/\\n/g,"\n");
  var content = jade.compile(jadetext,{})({content:ostext});
  res.send(content);
  log(req.query.page+".jade");
})

// return less file content
app.get('/less',function(req,res){
  log(req.query.page)
  var lesstext=fs.readFileSync(__dirname+'/public/less/'+req.query.page+".less").toString();
  less.render(lesstext, function (e, output) {
    res.send(output.css);
    log(req.query.page+".less");
  });
})

// handles jseditor submits
app.post('/submit',function(req,res){
  req.params=_.extend(req.params || {}, req.query || {}, req.body || {});
  log(req.params)
  fs.writeFile(global.vars.file,req.params.text,function(e,r){
    setTimeout(function(){
      var jadetext=fs.readFileSync(__dirname+'/public/jade/redirect.jade').toString();
      var content = jade.compile(jadetext,{})({url:req.params.callback});
      res.send(content);
    },global.vars.config.main.fscooldown);
  })
})

// app.get('/cmd', function(req,res){
//   var cmd=req.param('cmd');
//
//   function out(stdout) {
//     log("cmd: "+cmd);
//     log(stdout);
//     res.send(stdout);
//   }
//
//   (new SSH(ssh_option)).exec(cmd, {out: out}).start();
// })

// return motd for fileos
app.get('/info', function (req, res) {
  res.send('Welcome to FileOS'+"\n"+getFile(__dirname+'.motd'));
})

app.get('/', function (req, res) {
  req.query.page="index";
  var jadetext=fs.readFileSync(__dirname+'/public/jade/'+req.query.page+".jade").toString();
  var ostext=fs.readFileSync(global.vars.file).toString().replace(/\\n/g,"\n");
  var content = jade.compile(jadetext,{})({content:ostext});
  res.send(content);
  log(req.query.page+".jade");
})

// static file host
app.use(express.static('public/www'));

return app;
}
