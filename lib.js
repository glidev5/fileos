var SSH = require('simple-ssh');

//var needle=require('needle');
var htmlToText = require('html-to-text');
var Tail = require('tail').Tail;
var stripAnsi = require('strip-ansi');
var shortid = require('shortid');
var util = require('util');
var exec = require('child_process').exec;
var ini=require('ini')

var jade = require('jade');
var less = require('less');

var md5=require("md5-file");
var logcooldown=null;
var Q=require('q');
var runs=1;
var fs=require('fs');

//var beautify = require('js-beautify').js_beautify;
var nb = require('node-beautify');

// eval without the bracket hassle
function fneval(str){
  return eval("("+str+")");
}
exports.fneval=fneval;

// return file content as string
function getFile(filename){
  return fs.readFileSync(filename).toString();
}
exports.getFile=getFile;

// eval requre is like require, but done by eval, so that any chunk of any js file can be extrapolated
function evalRequire(file,that){
  evalInContext.call(that,getFile(file))
  function evalInContext(content){
    var fns=eval(content);
    for(var name in fns){
      if(!global[name]){
        that[name]=fns[name];
        //log(name)
      }
    }
  }
}
exports.evalRequire=evalRequire;

// global log method, customized for better perf
function log(text){
  setTimeout(function(){
    try{
      console.log(text);
      return fs.appendFile(config.main.log,text.toString()+"\n",function(e,r){})
    }catch(e){
      //console.log(e)
    }
  },logcooldown||5);
}
exports.log=log;

var debugmode=true;
function debug(text){
  if(debugmode){
    log(text);
  }
}
exports.debug=debug;

function tryLog(e){
  if(e){
    log(e);
  }
}

exports.tryLog=tryLog;

// log file content as log
function tryLogFile(filename){
  try{
    log(getFile(filename));
  }catch(e){}
}
exports.tryLogFile=tryLogFile;

function shortHand(o,cb){
  //log("shorthand")
  log(o.args[1])
  log(global.vars.config.shorthand[o.args[1]])
  o.data=o.data.replace(o.cmd.pattern,global.vars.config.shorthand[o.args[1]]+"");
  cb(null,o);
}
exports.shortHand=shortHand;

function shorthandDefine(o,cb){
  log("shorthandDefine")
  var filename=__dirname+'/.shorthand.ini';
  log(filename)
  o.data=o.data.replace(o.cmd.pattern,o.cmd.remain);
  var config2=ini.parse(fs.readFileSync(filename).toString());
  config2["shorthand"][o.args[1]]=o.args[2]+"";
  //log(config2)
  fs.writeFile(filename,ini.stringify(config2),function(e,r){
    tryLog(e);
    configReload(o,function(e,r){
      cb(null,o);
    })
  })
}
exports.shorthandDefine=shorthandDefine;

function configReload(o,cb){
  log("config reloaded")
  o.data=o.data.replace(o.cmd.pattern,o.cmd.remain);
  global.vars.load(function(e,r){
    cb(null,o);
  });
}

exports.configReload=configReload;

// runCmd is a route for running bash command in globalcmd
function runCmd(o,cb){
  //log(o.args)
  o.data=o.data.replace(o.cmd.pattern,o.cmd.remain);
  Q(o.args[2]).then(runcmd).then(function(sec){
    o.data+=sec.format();
    //log(o)
    cb(null,o);
  });
}
exports.runCmd=runCmd;

// run a single command and return command stdout
function runcmd(cmd){
  var deferred=Q.defer();
  log("running command: "+cmd)
  child = exec(cmd, function (error, stdout, stderr) {
    var sec=new sectionTemp(cmd,null,stdout);
    log(1)
    global.vars.secMgr[sec.id]=sec;
    log(2)
    deferred.resolve(sec);
  });
  return deferred.promise;
}
exports.runcmd=runcmd;

// section analyzes text and convert them to section objects
function section(o){
  var deferred = Q.defer();
  //console.log("analyzing sections...");
  // o.data -> o.section

  // let's recreate secMgr
  global.vars.secMgr={};
  var arg;
  var config=global.vars.config;
  //var content=(o.data.trim()).split(config.section.sectionDelimiter);
  var content=o.data.split(config.section.sectionDelimiter);
  //log(content)
  if(content){
    async.eachSeries(content,function(ctnt,cb){
      //ctnt=ctnt.trim();
      //log(global.vars.config.section.sectionPattern.exec(ctnt))
      if(arg=global.vars.config.section.sectionPattern.exec(ctnt)){
        //console.log(ctnt)
      //log("arg:"+arg)
        //sectionTemp(name,id,content,index,input)
        var sec=new sectionTemp(arg[2].trim(),arg[1].trim(),arg[3],arg['index'],arg['input']);
        //log("sec:"+sec)
        //if(!global.vars.secMgr[sec.id]){
          global.vars.secMgr[sec.id]=sec;
          //log(sec.format());
        //}
        cb();
      }
      else{
        cb();
      }
    },function(e,r){
      //log("secMgr:")
      //log(global.vars.secMgr)
      deferred.resolve(o);
    })
  }else{
    deferred.resolve(o);
  }
  return deferred.promise;
}
exports.section=section;

// this is the godly section instantiation code, don't play god
function sectionTemp(name,id,content,index,input){

  this.name=name||"new";
  this.id=id||shortid.generate();
  this.description="";
  this.cmd=this.name;
  this.lifetime=-1;
  this.config={};
  this.content=content||"";
  this.index=index||-1;
  this.raw=input||"";
  this.updates=0;

  var that=this;

  // to formatted string
  this.format=function(format){
    var result=format||global.vars.config.section.sectionFormat;

    var fields={
      "name":"name",
      "id":"id",
      "content":"content"
    }

    _.forEach(fields,function(field){
      var reg=new RegExp("{{\\\$"+field+"}}","g");  // ex {{$name}} --> this['name']{{
      result=result.replace(reg,that[field]);
    })

    result=result.replace(/\\n/g,"\n");

    //log(result)
    return result;
  }

  // to raw string
  this.toString=function(format){
    return this.format(format||global.vars.config.section.sectionString);
  }
}
exports.sectionTemp=sectionTemp;

// regenerate os.txt
function regenos(o){
  var deferred = Q.defer();
  var content="";
  async.eachSeries(secMgr,function(sec,cb){
    content+=sec.format().trim().replace(/\\n/g,'\n')+"\n";
    deferred.resolve(o);
  },function(e,r){
    return fs.writeFile(file,content,function(e,r){
      setTimeout(deferred.resolve,global.vars.config.main.fscooldown);
    })
  });
  return deferred.promise;
}
exports.regenos=regenos;

// check if file changed,
//  keep a stack of last modified time and last md5 hash,
//  assuming file paths are unique
var last={};
last.lasttime={};
last.lasthash={};
function filechanged(file){
  var stats=fs.statSync(file);
  if(last.lasttime[file]&&stats.mtime==last.lasttime[file]){
    return false;
  }
  last.lasttime[file]=stats.mtime

  var md5hash=md5(file);
  if(last.lasthash[file]&&md5hash==last.lasthash[file]){
    return false;
  }
  last.lasthash[file]=md5hash;

  return true;
}
exports.filechanged=filechanged;

// function trycb(e,r,cb){
//   try{
//     if(e){
//       log(e);
//     }
//     cb(e,r);
//   }catch(e){
//   }
// }
//

function trycb(cb){
  try{
    cb();
  }catch(e){
    if(e)
      log(e);
  }
}
exports.trycb=trycb;

// save section content to file
function saveSection(o,cb){
  // o.filename is the filename of the section to be saved to
  // o.filename=seccmd.argument||sec.name
  // o.content=sec.format();
  //var deferred=Q.defer();
  log(o.args)
  o.data=o.data.replace(o.cmd.pattern,o.cmd.remain);
  o.sec.content=o.sec.content.replace(o.cmd.pattern,o.cmd.remain);
  o.filename=(o.args[2]||o.sec.name||"untitled").replace(/cat /,"").replace(/touch /,"").trim();
  fs.writeFile(o.filename,o.sec.content,function(e,r){
    setTimeout(function(){cb(null,o)},global.vars.config.main.fscooldown);
  })
  //return deferred.promise;
}
exports.saveSection=saveSection;

// remove a section
function removeSection(o,cb){
  log("remove section...");
  //log(o)
  o.data=o.data.replace(o.sec.raw.trimLeft()+global.vars.config.section.sectionDelimiter,"");
  //delete global.vars.secMgr[o.sec.id];
  //log(o.sec)
  //log("remove section data ******************")
  //log(o.data)
  cb(null,o);
}
exports.removeSection=removeSection;

function archiveSection(sec,args,cb){
  // todo: complete archiveSection
  cb();
}
exports.archiveSection=archiveSection;

function stashSection(o){
  var deferred=Q.defer();
  archiveSection(o.sec,function(e,r){
    o.data.replace(o.sec.raw,"");
    delete o.secMgr[o.sec.id];
    deferred.resolve(o);
  });
  return deferred.promise;
}
exports.stashSection=stashSection;

// create a new section
function newSection(o,cb){
  log("new")
  var sec=new sectionTemp();
  o.data=o.data.replace(o.cmd.pattern,o.cmd.remain);
  o.data+=sec.format()
  global.vars.secMgr[sec.id]=sec;
  cb(null,o);
}
exports.newSection=newSection;

// grab all config json
function getConfig(o,cb){
  log("getConfig")
  //var deferred=Q.defer();
  var resu=JSON.stringify(global.vars.config,null,2).replace(/{{.*}}/,"\{\{\}\}");
  var sec=new sectionTemp("config",null,resu,null,null);
  o.data=o.data.replace(o.cmd.pattern,o.cmd.remain);
  o.data+=sec.format();
  global.vars.secMgr[sec.id]=sec;
  cb(null,o);
  //deferred.resolve(o);
  //return deferred.promise;
}
exports.getConfig=getConfig;

function exitApp(o,cb){
  o.data=o.data.replace(o.cmd.pattern,o.cmd.remain);
  global.vars.config.main.nextwatch=false;
  cb(null,o);
}
exports.exitApp=exitApp;

function copySection(o){
  var deferred=Q.defer();
  var sec=new sectionTemp(o.sec.name,null.o.sec.content);
  o.data+=sec.format();
  secMgr[sec.id]=sec;
  deferred.resolve(o);
  return deferred.promise;
}
exports.copySection=copySection;

function reloadSection(o){
  var deferred=Q.defer();
  runcmd(o.sec.name,function(e,sec){
    o.sec.conent=sec.content;
    data=data.replace(sec.raw,sec.format());
    o.sec.raw=o.sec.format();
    deferred.resolve(o);
  });
  return deferred.promise;
}
exports.reloadSection=reloadSection;

function removeSecCmd(o){
  var deferred=Q.defer();
  o.sec.content=o.sec.content.replace(o.cmd.pattern,o.cmd.remain);
  o.sec.raw=o.sec.format();
  o.data=o.data.replace(o.cmd.pattern,o.cmd.remain);
  deferred.resolve(o);
  return deferred.promise;
}
exports.removeSecCmd=removeSecCmd;

function secMgrAdd(sec){
  secMgr[sec.id]=sec;
}
exports.secMgrAdd=secMgrAdd;

function secMgrRemove(sec){
  delete secMgr[sec.id];
}
exports.secMgrRemove=secMgrRemove;

function replaceSection(o){
  // o.sec
  // o.data
  // o.content
  var deferred=Q.defer();
  o.data=o.data.replace(o.sec.raw,o.content);
  o.sec.raw=o.content;
  fs.writeFile(global.var.file,o.data,function(e,r){
    deferred.resolve(o);
  });
  return deferred.promise;
}

exports.replaceSection=replaceSection;

function checkSectionPattern(){
  var sec=new sectionTemp();
  sec.content="section test content";
  var hay=sec.format();
  return !!global.vars.config.section.sectionPattern.exec(hay);
}
exports.checkSectionPattern=checkSectionPattern;


function replace(o,cb){
  Q(o)
  .then(removeSecCmd)
  .then(function(o){
    o.data=o.data.replace(o.args[1],o.args[2]);
  })
  .then(function(){
    cb(null,o)
  })
}
exports.replace=replace;

function replaceAll(o,cb){
  Q(o)
  .then(removeSecCmd)
  .then(function(o){
    var re = new RegExp(o.args[1],"g");
    o.data=o.data.replace(re,o.args[2]);
  })
  .then(function(){
    cb(null,o)
  })
}
exports.replaceAll=replaceAll;

function beautifySection(o,cb){
  Q(o)
  .then(removeSecCmd)
  .then(function(o){
    //o.data=o.data.replace(o.sec.raw,beautify(o.data, { indent_size: 2 }));
    o.data = o.data.replace(o.sec.content, nb.beautifyJs(o.sec.content));
  })
  .then(function(){
    cb(null,o);
  })
}
exports.beautify=beautifySection;
