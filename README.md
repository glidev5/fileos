# FileOS
FileOS is an operating system built on top of any os.txt file. The entire OS operation can be achieved by interacting with a live os.txt file.

## Feature
Ssh has enabled most of the communication between user and server. However, ssh has several major draw backs that fileos is intended to fix.
Unlike ssh where the commandline are based on linear flow, where the past logs are scrolled upward and the command line prompt is linear from left to right, fileos does this differently.
Past logs in fileos are presented as sections. Command line prompt is also replaced by editing the os.txt file anywhere.
FileOS also makes os.txt being watched by a daemon to become live and interactive.

## Installation
### WIP

## Usage
### instructions
### WIP

## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History
* 0.0.8  added https basic auth
* 0.0.7  web portal working, keyboard working
* 0.0.6  SectionCmd done, shorthand done, beautify, replace, replaceAll done
* 0.0.5  GlobalCmd done, working on {{remove}}
* 0.0.4  shorthandDefine
* 0.0.3  globalcmd, sectioncmd, shorthand
* 0.0.2  bash shell commands
* 0.0.1  git init

## Credits
 * Thanks to Node.js for awesome modules.

## License
 * [Apache License](http://www.apache.org/licenses/LICENSE-2.0)
